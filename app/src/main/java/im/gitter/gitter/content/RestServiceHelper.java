package im.gitter.gitter.content;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.UiThread;
import android.util.Log;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import static im.gitter.gitter.content.RestService.ORIGINAL_INTENT_INTENT_KEY;

public class RestServiceHelper {

    public static final String INTENT_ACTION = "im.gitter.gitter.rest";
    private static final String TAG = RestServiceHelper.class.getSimpleName();
    private static final String PENDING_REQUEST_KEY_INTENT_KEY = "PENDING_REQUEST_KEY";
    public static final String REQUEST_ID_INTENT_KEY = "REQUEST_ID";
    public static final String RESULT_CODE_INTENT_KEY = "RESULT_CODE";
    private static RestServiceHelper instance;
    private Map<String, Long> pendingRequests = new HashMap<>();

    private RestServiceHelper() {}

    @UiThread
    public static RestServiceHelper getInstance() {
        if(instance == null){
            instance = new RestServiceHelper();
        }

        return instance;
    }

    public long getRoomList(Context context) {
        return startService(context, RestService.RequestType.GetUserRooms);
    }

    public long getRoom(Context context, String roomId) {
        LinkedHashMap<String, String> args = new LinkedHashMap<>();
        args.put(RestService.ROOM_ID_INTENT_KEY, roomId);
        return startService(context, RestService.RequestType.GetRoom, args);
    }

    public long joinRoom(Context context, String roomId) {
        LinkedHashMap<String, String> args = new LinkedHashMap<>();
        args.put(RestService.ROOM_ID_INTENT_KEY, roomId);
        return startService(context, RestService.RequestType.JoinRoom, args);
    }

    public long leaveRoom(Context context, String roomId) {
        LinkedHashMap<String, String> args = new LinkedHashMap<>();
        args.put(RestService.ROOM_ID_INTENT_KEY, roomId);
        return startService(context, RestService.RequestType.LeaveRoom, args);
    }

    public long getUser(Context context, String userId) {
        LinkedHashMap<String, String> args = new LinkedHashMap<>();
        args.put(RestService.USER_ID_INTENT_KEY, userId);
        return startService(context, RestService.RequestType.GetUser, args);
    }

    public long getAdminGroups(Context context) {
        return startService(context, RestService.RequestType.GetAdminGroups);
    }

    public long getUserGroups(Context context) {
        return startService(context, RestService.RequestType.GetUserGroups);
    }

    public long getRoomsForGroup(Context context, String groupId) {
        LinkedHashMap<String, String> args = new LinkedHashMap<>();
        args.put(RestService.GROUP_ID_INTENT_KEY, groupId);
        return startService(context, RestService.RequestType.GetRoomsForGroup, args);
    }

    public long sendMessageToRoom(Context context, String message, String roomId) {
        LinkedHashMap<String, String> args = new LinkedHashMap<>();
        args.put(RestService.MESSAGE_INTENT_KEY, message);
        args.put(RestService.ROOM_ID_INTENT_KEY, roomId);
        return startService(context, RestService.RequestType.SendMessageToRoom, args);
    }

    public long markAllAsRead(Context context, String roomId) {
        LinkedHashMap<String, String> args = new LinkedHashMap<>();
        args.put(RestService.ROOM_ID_INTENT_KEY, roomId);
        return startService(context, RestService.RequestType.MarkAllAsRead, args);
    }

    private long startService(Context context, RestService.RequestType requestType) {
        return startService(context, requestType, new LinkedHashMap<String, String>());
    }

    private long startService(Context context, RestService.RequestType requestType, LinkedHashMap<String, String> args) {
        String pendingRequestKey = generatePendingRequestKey(requestType, args);
        if (pendingRequests.containsKey(pendingRequestKey)) {
            Log.i(TAG, "pending request is already in flight, returning pending request id");
            return pendingRequests.get(pendingRequestKey);
        }

        Context applicationContext = context.getApplicationContext();

        long requestId = generateRequestID();

        Intent intent = new Intent(applicationContext, RestService.class);
        intent.putExtra(RestService.REQUEST_ID_INTENT_KEY, requestId);
        intent.putExtra(RestService.REQUEST_TYPE_INTENT_KEY, requestType);
        for (String key : args.keySet()) {
            intent.putExtra(key, args.get(key));
        }
        intent.putExtra(RestService.SERVICE_CALLBACK_INTENT_KEY, createServiceCallback(applicationContext));
        intent.putExtra(PENDING_REQUEST_KEY_INTENT_KEY, pendingRequestKey);

        applicationContext.startService(intent);
        pendingRequests.put(pendingRequestKey, requestId);

        return requestId;
    }

    private String generatePendingRequestKey(RestService.RequestType requestType, LinkedHashMap<String, String> args) {
        StringBuilder builder = new StringBuilder(requestType.name());
        for (String key : args.keySet()) {
            builder.append(":");
            builder.append(args.get(key));
        }
        return builder.toString();
    }

    private long generateRequestID() {
        return UUID.randomUUID().getLeastSignificantBits();
    }

    private ResultReceiver createServiceCallback(final Context applicationContext) {
        return new ResultReceiver(null) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                Intent origIntent = resultData.getParcelable(ORIGINAL_INTENT_INTENT_KEY);

                if (origIntent != null) {
                    String pendingRequestKey = origIntent.getStringExtra(PENDING_REQUEST_KEY_INTENT_KEY);
                    pendingRequests.remove(pendingRequestKey);

                    long requestId = origIntent.getLongExtra(RestService.REQUEST_ID_INTENT_KEY, 0);

                    Intent resultBroadcast = new Intent(INTENT_ACTION);
                    resultBroadcast.putExtra(REQUEST_ID_INTENT_KEY, requestId);
                    resultBroadcast.putExtra(RESULT_CODE_INTENT_KEY, resultCode);

                    applicationContext.sendBroadcast(resultBroadcast);
                }
            }
        };
    }
}
