package im.gitter.gitter.network;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;

import com.android.volley.Response;

import org.json.JSONException;
import org.json.JSONObject;

import im.gitter.gitter.content.ModelFactory;
import im.gitter.gitter.models.Group;
import im.gitter.gitter.models.Room;
import im.gitter.gitter.models.User;

public abstract class RoomRequest extends ApiRequest<String> {

    private final ModelFactory modelFactory = new ModelFactory();
    private final ContentResolver contentResolver;

    public RoomRequest(Context context, String path, com.android.volley.Response.Listener<String> listener, com.android.volley.Response.ErrorListener errorListener) {
        this(context, Method.GET, path, null, listener, errorListener);
    }

    public RoomRequest(Context context, int method, String path, JSONObject jsonRequest, com.android.volley.Response.Listener<String> listener, com.android.volley.Response.ErrorListener errorListener) {
        super(context, method, path, jsonRequest, listener, errorListener);
        this.contentResolver = context.getContentResolver();
    }

    @Override
    protected String parseJsonInBackground(String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);

        Room roomModel = modelFactory.createRoom(jsonObject);
        ContentValues room =  roomModel.toContentValues();
        ContentValues user = null;
        ContentValues group = null;

        if (!jsonObject.isNull("user")) {
            user = modelFactory.createUser(jsonObject.getJSONObject("user")).toContentValues();
        }

        if (!jsonObject.isNull("group")) {
            group = modelFactory.createGroup(jsonObject.getJSONObject("group")).toContentValues();
        }

        writeToDatabaseInBackground(contentResolver, room, user, group);

        return roomModel.getId();
    }

    protected abstract void writeToDatabaseInBackground(ContentResolver contentResolver, ContentValues room, ContentValues user, ContentValues group);
}
